import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { AboutusComponent } from './aboutus/aboutus.component';
import { EventComponent } from './event/event.component';
import { CartComponent } from './cart/cart.component';
import { ShowusersComponent } from './showusers/showusers.component';

const routes: Routes = [
  {path:'', component:NavbarComponent},
  {path:'Home', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'logout', canActivate:[authGuard], component:LogoutComponent},
  {path:'aboutus', canActivate:[authGuard], component:AboutusComponent},
  {path:'event', canActivate:[authGuard], component:EventComponent},
  {path:'cart', canActivate:[authGuard], component:CartComponent},
  {path:'showusers', canActivate:[authGuard], component:ShowusersComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
