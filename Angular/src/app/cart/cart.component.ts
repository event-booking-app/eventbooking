import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  // cartItems: any;
  // cartEvents: any;
  localStorageData: any;
  email: any;
  total: number;
  events: any;

  constructor(private service: UserService) {
    this.total = 0;
    this.email = localStorage.getItem('email');
    this.events = service.getCartItems();
    this.events.forEach((element: any) => {
      this.total = this.total + parseInt(element.price);
    });
  }
  ngOnInit() {

  }
  deleteCartEvent(event: any) {
    const i = this.events.findIndex((element: any) => {
      return element.id == event.id;
    });
    this.events.splice(i, 1);
    this.total = this.total - event.price;
  }
  purchase() {
    this.events = [];
    this.total = 0;
    this.service.setCartItems();
  }
}