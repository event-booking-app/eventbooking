import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

interface Event {
  id: number;
  name: string;
  flipname: string;
  description: string;
  price: number;
  imgsrc: string;
}

// // interface CartItem {
// //   event: event;
// //   quantity: number;
// }
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrl: './event.component.css'
})
export class EventComponent implements OnInit {
  events: Event[];
  // email:any;

  //AddToCart
  // cartItems: any;

  constructor(private service: UserService) {

    //AddToCart
    // this.cartItems = [];
    // this.email = localStorage.getItem('email');

    this.events = [

      {
        id: 1001,
        name: "Goa",
        flipname: "A Love Affair with the Hills",
        description: "Darjeeling offers a romantic.",
        price: 450.99,
        imgsrc: "assets/Images/event1.jpg"
      },

      {
        id: 1002,
        name: "Gujarat",
        flipname: "A lover's tale in stone",
        description: "An architectural marvel in Agra, India, is a symbol of eternal love. It stands as a testament to their love story. Visiting at sunrise or sunset offers a breathtaking view of its white marble facade glowing in the changing light, creating a romantic ambiance. Walking through its gardens and admiring its intricate details, one can't help but feel the enduring love that inspired its creation.",
        price: 450.99,
        imgsrc: "../../assets/Images/event12.jpg"
      },

      {
        id: 1003,
        name: "Rajput",
        flipname: "A Symphony of Love",
        description: "Manali is a haven for lovers seeking a romantic escape. From its snow-capped peaks to its lush valleys, every corner of Manali is a canvas painted with love. Walk hand in hand through its quaint streets, breathe in the crisp mountain air, and let the magic of Manali weave a tale of romance that will linger in your hearts forever.",
        price: 450.99,
        imgsrc: "../../assets/Images/event4.jpg"
      },

      {
        id: 1004,
        name: "Kashmir",
        flipname: "Love Paradise",
        description: "Nestled in the lap of the Bay of Bengal, With their pristine beaches, azure waters, and lush greenery, these islands offer the perfect backdrop for a romantic getaway. Walk along the shore hand in hand, indulge in a candlelit dinner by the beach, and let the magic of the islands kindle the flames of love",
        price: 450.99,
        imgsrc: "../../assets/Images/event5.jpg"
      },


    ];
  }
  ngOnInit() {

  }
  Book(event: Event) {
    // Call your service method to book the event
    this.service.Book(event);
  }

  // addToCart(event: any){
  //   this.service.addToCart(event);
  // this.cartItems.push(event);
  // localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
}