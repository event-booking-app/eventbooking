import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  user: any;
  protected aFormGroup! : FormGroup;

  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LeLjWgpAAAAAOK4Ws8MXmXZNqsjC57pChxWpJig";
 

  //Dependency Injection for EmpService, Router
  constructor(private service: UserService, private formBuilder: FormBuilder,  private router: Router) {   
    this.aFormGroup = this.formBuilder.group({recaptcha: ['', Validators.required]});
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }


  ngOnInit(){
    
  }
  async loginSubmit(loginForm: any) {
    if (loginForm.email == 'Admin' && loginForm.password == 'admin') {           
      //Setting the isUserLoggedIn variable value to true under EmpService
      this.service.setIsUserLoggedIn();
      localStorage.setItem("email", loginForm.email);
       this.router.navigate(['aboutus']);
      
    } else {
      this.user = null;

      await this.service.userLogin(loginForm.email, loginForm.password).then((data: any) => {
        console.log(data);
        this.user = data;
      });

      if (this.user!= null) {
        this.service.setIsUserLoggedIn();        
        localStorage.setItem("email", loginForm.email);
        this.router.navigate(['aboutus']);
        

       
      } else {
        
        alert('Invalid Credentials');
      }
    }
  }
}