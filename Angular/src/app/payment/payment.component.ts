import { Component, ElementRef, OnInit } from '@angular/core';
import { UserService } from '../user.service'; 
declare var jQuery: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  payments: any;
  editPayment: any; 
user: any;
email: any;
booking: any;

  constructor(private Service: UserService) { 
    this.editPayment = {
      PaymentId: '',
      paymentAmount:'',
      paymentStatus: '',
      paymentDate:'',
     
    };
  }

  ngOnInit() {
    this.Service.getPayments().subscribe((data: any) => { this.payments = data; });
  }
  editPayments(payment: any) {
    console.log(payment);

    //Employee data binding to the editEmp variable for 2-way databinding
    this.editPayment = payment;

    //2. Launching the Modal Dialog Box
    jQuery('#myModal').modal('show');
  }
  updatePayment() {
    console.log(this.editPayment);
    this.Service.updatePayment(this.editPayment).subscribe((data: any) => { console.log(data); });
  }

  deletePayment(payment: any) {
    this.Service.deletePayment(payment.paymentId).subscribe((data: any) => { console.log(data); });

    const i = this.payments.findIndex((element: any) => {
      return element.paymentId == payment.paymentId;
    });

    this.payments.splice(i, 1);

    alert('User Deleted Successfully!!!');
  }
}