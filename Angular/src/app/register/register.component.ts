import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit{
  protected aFormGroup! : FormGroup;
     
  user:any;
  name: any;
  gender: any;
  phoneNumber: any;
  email: any;
  password: any;
  confirmPassword:any;
  states:any;
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey:string = "6Le1mmgpAAAAAG7i2p2-tYWDww-9FgeVYOaY4d1V" ;

  constructor( private router: Router, private toastr: ToastrService,private Service:UserService, private formBuilder: FormBuilder) {
    this.user= {
      name: '',
      email: '',
      password: '',
    };
    
    this.aFormGroup = this.formBuilder.group({recaptcha: ['', Validators.required]});
  }
 


  ngOnInit() {
    
  }
  onCaptchaResolved(event: any) {
    // Handle captcha resolved event
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }
  registerSubmit(regForm: any) {
    if (this.user.password !== this.confirmPassword) {
      console.log('Password and Confirm Password must be the same.');
    }
  console.log(regForm);
    if (!this.validateForm()) {
      return;
    }
    this.user.name = regForm.name;
    this.user.gender = regForm.gender;
    this.user.phoneNumber = regForm.phoneNumber;
    this.user.states = regForm.states;
    this.user.email = regForm.email;
    this.user.password = regForm.password;
  
    console.log(this.user);
    

    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.Service. regsiterUser(this.user).subscribe((data: any) => { console.log(data); });
    this.router.navigate(['login']);
    
  }

  private validateForm(): boolean {

    if (!this.name || !this.email || !this.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }

  private generateUserId(): number {
    return Math.floor(Math.random() * 1000) + 1;
  }
}