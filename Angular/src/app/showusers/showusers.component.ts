import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

declare var jQuery: any;

@Component({
  selector: 'app-showusers',
  templateUrl: './showusers.component.html',
  styleUrls: ['./showusers.component.css']
})
export class ShowusersComponent implements OnInit {
  users: any;
  payments: any;
  editUser: any;
  editPayment: any;
  email: any;

  constructor(private service: UserService) {
    // Getting emailId from LocalStorage
    this.email = localStorage.getItem('email');

    // for 2-way databinding with dialog box
    this.editUser = {
      userId: '',
      name: '',
      gender: '',
      states: '',
      email: '',
      password: '',
      phoneNumber: '',
    };

    this.editPayment = {
      PaymentId: '',
      paymentAmount: '',
      paymentStatus: '',
      paymentDate: '',
    };
  }

  ngOnInit() {
    this.service.getAllUsers().subscribe((data: any) => { this.users = data; });
    this.service.getPayments().subscribe((data: any) => { this.payments = data; });
  }

  editUsers(user: any) {
    console.log(user);
    this.editUser = user;
    jQuery('#myModal').modal('show');
  }

  editPayments(payment: any) {
    console.log(payment);
    this.editPayment = payment;
    jQuery('#myModal').modal('show');
  }

  updateUser() {
    console.log(this.editUser);
    this.service.updateUser(this.editUser).subscribe((data: any) => { console.log(data); });
  }

  updatePayment() {
    console.log(this.editPayment);
    this.service.updatePayment(this.editPayment).subscribe((data: any) => { console.log(data); });
  }

  deleteUser(user: any) {
    this.service.deleteUser(user.userId).subscribe((data: any) => { console.log(data); });
    const i = this.users.findIndex((element: any) => {
      return element.userId == user.userId;
    });
    this.users.splice(i, 1);
    alert('User Deleted Successfully!!!');
  }

  deletePayment(payment: any) {
    this.service.deletePayment(payment.paymentId).subscribe((data: any) => { console.log(data); });
    const i = this.payments.findIndex((element: any) => {
      return element.paymentId == payment.paymentId;
    });
    this.payments.splice(i, 1);
    alert('Payment Deleted Successfully!!!');
  }
  hasPaymentForUser(user: any): boolean {
    return this.payments.some((payment: { userId: any; }) => payment.userId === user.userId);
  }
  
}