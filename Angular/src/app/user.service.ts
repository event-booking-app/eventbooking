import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  cartItems: any;
  loginStatus: any;
  isUserLoggedIn: any;

  constructor(private http:HttpClient) { 
    this.loginStatus = new Subject();
    this.isUserLoggedIn = false;

    this.cartItems = [];
  }

  getAllUsers():any{
    return this.http.get('http://localhost:8085/getUsers');
  }
  regsiterUser(user: any): any {
    return this.http.post('http://localhost:8085/addUser', user);
  }
  userLogin(email: any, password: any): any {
    return this.http.get('http://localhost:8085/userLogin/' + email + '/' + password).toPromise();
  }
  deleteUser(userId: any) {
    return this.http.delete('http://localhost:8085/deleteUserById/' + userId);
  }
  updateUser(user: any) {
    return this.http.put('http://localhost:8085/updateUser', user);
  }
  Book(event: any) {
    this.cartItems.push(event);
  }
  getCartItems() {
    return this.cartItems;
  }
  setCartItems() {
    this.cartItems.splice()
  }
  getPayments(): any {
    return this.http.get('http://localhost:8085/getPayments');
  }
  deletePayment(paymentId: any) {
    return this.http.delete('http://localhost:8085/deletePaymentById/' + paymentId);
  }
  updatePayment(Payment: any) {
    return this.http.put('http://localhost:8085/updatePayment', Payment);
  }
  //login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
}