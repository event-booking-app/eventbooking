package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Admin;

@Repository
public class AdminDao {
     @Autowired
     AdminRepository adminRepository;
     
     public List<Admin> getAdmin(){
    	 return adminRepository.findAll();
     }
     public Admin getAdminById(int adminId){
    	 return adminRepository.findById(adminId).orElse(null);
     }
     public Admin getAdminByNmae(String username){
    	 return adminRepository.findByName(username);
     }
     public Admin adminLogin(String email,String password){
    	 Admin admin = adminRepository.adminLogin(email,password);
    	 return admin;
     }
     public Admin updateAdmin(Admin admin){
    	 return adminRepository.save(admin);
     }
     public void deleteAdminById(int adminId){
    	 adminRepository.deleteById(adminId);
     }
	public Admin addAdmin(Admin admin) {
		Admin savedAdmin = adminRepository.save(admin);
		return savedAdmin;
	}
     
}