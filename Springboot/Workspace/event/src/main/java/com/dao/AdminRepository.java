package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Admin;

public interface AdminRepository extends JpaRepository<Admin,Integer>{
	
	@Query("from Admin where username = :adminName")
	Admin findByName(@Param("adminName")String adminName);
	
	@Query("from Admin where email = :email and password = :password")
	Admin adminLogin(@Param("email")String email, @Param("password")String password);

}