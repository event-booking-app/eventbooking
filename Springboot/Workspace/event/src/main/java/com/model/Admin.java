package com.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity

public class Admin {
    @Id
    @GeneratedValue
    private int adminId;
    private String username;
    private String password;
    private String email;
    private String fullName;
    private String phonenumber;  
    
    @OneToMany(mappedBy = "admin")
    private List<User> users;

    @OneToMany(mappedBy = "admin")
    private List<Event> events; 

    @OneToMany(mappedBy = "admin")
    private List<Booking> bookings;

    @OneToMany(mappedBy = "admin")
    private List<Payment> payments;
    
    public Admin() {
    	 // Default constructor
    }
   
    	public Admin (int adminId,String username,String password, String email,String fullName,String phonenumber){
    		
        	this.adminId=adminId;
        	this.username=username;
        	this.password=password;
        	this.email=email;
        	this.fullName=fullName;
        	this.phonenumber=phonenumber;
    	}
    	
    	public int getAdminId() {
			return adminId;
		}
		public void setAdminId(int adminId) {
			this.adminId = adminId;
		}
    	
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getFullName() {
			return fullName;
		}
		public void setFullName(String fullName) {
			this.fullName = fullName;
		}
		public String getPhonenumber() {
			return phonenumber;
		}
		public void setPhonenumber(String phonenumber) {
			this.phonenumber = phonenumber;
		}
   
}