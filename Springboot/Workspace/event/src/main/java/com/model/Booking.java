package com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;



//Booking.java
@Entity

public class Booking {
 @Id
 @GeneratedValue
 private int bookingId;
 private LocalDate bookingDate;
 
 @ManyToOne
 @JoinColumn(name = "user_id")
 private User user; ;

 @ManyToOne
 @JoinColumn(name = "event_id")
 private Event event;
 
 @ManyToOne
 @JoinColumn(name = "admin_id")
 private Admin admin;  

 @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL)
 private List<Payment> payments;
 
 public Booking() {
     // Default constructor
 }

 public Booking(LocalDate bookingDate, User user, Event event) {
     this.bookingDate = bookingDate;
     this.user = user;
     this.event = event;
 }
 
public LocalDate getBookingDate() {
	return bookingDate;
}

public void setBookingDate(LocalDate bookingDate) {
	this.bookingDate = bookingDate;
}


}