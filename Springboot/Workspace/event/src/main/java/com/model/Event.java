package com.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity

public class Event {
 @Id
 @GeneratedValue
 private int eventId;
 private String eventName;
 private LocalDate eventDate;
 private String eventLocation;

 @ManyToOne
 @JoinColumn(name = "admin_id")
 private Admin admin;
 
 @OneToMany(mappedBy = "event")
 private List<Booking> bookings;
 
 public Event() {
     // Default constructor
 }

 public Event(String eventName, LocalDate eventDate, String eventLocation) {
     this.eventName = eventName;
     this.eventDate = eventDate;
     this.eventLocation = eventLocation;
 }
 
 public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
 
public String getEventName() {
	return eventName;
}

public void setEventName(String eventName) {
	this.eventName = eventName;
}

public LocalDate getEventDate() {
	return eventDate;
}

public void setEventDate(LocalDate eventDate) {
	this.eventDate = eventDate;
}

public String getEventLocation() {
	return eventLocation;
}

public void setEventLocation(String eventLocation) {
	this.eventLocation = eventLocation;
}

 
}