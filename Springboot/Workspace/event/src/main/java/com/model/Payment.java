package com.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//Payment.java
@Entity
public class Payment {
	@Id
	@GeneratedValue
	private int paymentId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "booking_id")
	private Booking booking;

	 @ManyToOne
	    @JoinColumn(name = "admin_id")
	    private Admin admin;
	 
	private BigDecimal paymentAmount;
	private String paymentStatus;
	private LocalDateTime paymentDate;

	public Payment() {
		// Default constructor
	}

	public Payment(Booking booking, BigDecimal paymentAmount, String paymentStatus, LocalDateTime paymentDate) {
		this.user = user;
		this.booking = booking;
		this.paymentAmount = paymentAmount;
		this.paymentStatus = paymentStatus;
		this.paymentDate = paymentDate;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public LocalDateTime getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDateTime paymentDate) {
		this.paymentDate = paymentDate;
	}

}