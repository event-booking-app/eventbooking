package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AdminDao;
import com.model.Admin;

@RestController
public class AdminController {
     
	@Autowired
	private AdminDao adminDao;
	
	@GetMapping("/getAdmin")
	public List<Admin> getAdmin(){
		return adminDao.getAdmin();
	}
	@GetMapping("getAdminById/{adminId}")
	public Admin getAdminById(@PathVariable int adminId){
		return adminDao.getAdminById(adminId);
	}
	@GetMapping("getAdminByName/{username}")
	public Admin getAdminByName(@PathVariable String username){
		return adminDao.getAdminByNmae(username);
	}
	@GetMapping("adminLogin/{email}/{password}")
	public Admin adminLogin(@PathVariable String email,@PathVariable String password){
		return adminDao.adminLogin(email,password);
	}
	@PostMapping("addAdmin")
		public Admin addAdmin(@RequestBody Admin admin){
			return adminDao.addAdmin(admin);
	}
	@PutMapping("updateAdmin")
	public Admin ubdateAdmin (@RequestBody Admin admin) {
		return adminDao.updateAdmin(admin);
	}
	@DeleteMapping("deleteAdminById/{admin}")
	public String deleteAdminById(@PathVariable int adminId) {
		adminDao.deleteAdminById(adminId);
		return "Admin with adminId:" + adminId + "Delete Successfully!!!";
	}
}